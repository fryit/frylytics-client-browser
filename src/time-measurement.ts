import * as Utils from './utils';

export class TimeMeasurement {
    public readonly id: string;

    private _start: number;
    public getStart(): number { return this._start; }

    private _end: number;
    public getEnd(): number { return this._end; }

    public get value(): number {
        return this._end - this._start;
    }

    constructor(start: number = NaN, end: number = NaN) {
        this.id = Utils.ID(8);
        this._start = typeof start !== 'number' ? NaN : start;
        this._end = typeof end !== 'number' ? NaN : end;
    }

    public start(): TimeMeasurement {
        this._start = performance.now();
        return this;
    }
    public end(): TimeMeasurement {
        this._end = performance.now();
        return this;
    }
}