import { JSONObject } from './json';

export interface BackendOptions {
    baseURL: string;
    environment: string;
}

export class Backend {
    public readonly options: BackendOptions;

    constructor(options: BackendOptions) {
        this.options = options;
    }

    public endpointURL(endpoint: string, baseURL?: string, stage?: string): string {
        const base = baseURL || this.options.baseURL;
        const env = stage || this.options.environment;
        return `${base}${env}/${endpoint}`
    }

    public async send(event: JSONObject): Promise<any>;
    public async send(events: JSONObject[]): Promise<any>
    public async send(event: JSONObject|JSONObject[]): Promise<any> {}
}

export class AWSBackend extends Backend {
    public async send(event: JSONObject): Promise<any>;
    public async send(events: JSONObject[]): Promise<any>;
    public async send(event: JSONObject|JSONObject[]): Promise<any> {
        const parameters: RequestInit = {
          method: 'POST',
          mode: 'cors',
          cache: 'no-cache',
          //credentials: 'omit', // include, *same-origin, omit
          headers: { 'Content-Type': 'application/json' },
          //redirect: 'follow', // manual, *follow, error
          //referrer: 'no-referrer', // no-referrer, *client
          // body data type must match "Content-Type" header
          body: JSON.stringify(typeof event.length === 'number' ? event : [event])
        }
        const response = await fetch(this.endpointURL('events/'), parameters)
        const data = await response.json();
        
        if (!response.ok) {
            const message = response.statusText || data.error.message;
            throw new Error(message);
        }

        return data;
    }
}
