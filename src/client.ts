import { Backend } from './backend';
import { AnalyticsEvent, AnalyticsEventCategory, AnalyticsEventValue } from './analytics-event';
import { TimeMeasurement } from './time-measurement';

/*
const backend = AWSBackend({
    baseURL: 'https://2ore5fkn11.execute-api.eu-west-1.amazonaws.com/',
    environment: 'dev'|'prod'|'test'
});
const analytics = new Analytics(backend, {
    project: 'EAS',
    release: '1.0.0');
analytics.captureEvent('logged-in');

analytics.captureStart('form-item-render');
analytics.captureEnd('form-item-render', {});

const measurement = analytics.captureStart();
analytics.captureEnd('form-item-render', measurement, {});
...
...
// At some point collected events should be dispatched to API
analytics.startDispatching(5000);
analytics.stopDispatching();
*/

export interface Environment {
    client: string;
    organisation?: string;
    project?: string;
    release?: string;
    source?: string;
    tags: {[K: string]: string};
    user: {[K: string]: string};
}

export class Analytics {
    private static ENVIRONMENT: Environment = {
        client: window.navigator.userAgent,
        organisation: undefined,
        project: undefined,
        release: undefined,
        source: undefined,
        tags: {},
        user: {}
    };

    private backend: Backend;
    private events: AnalyticsEvent[] = [];
    private peformance: { [K: string]: TimeMeasurement } = {};

    private dispatcher = (that: Analytics): void => {
        if (that.events.length === 0) { return; }
        const events = that.events.map(e => e.toJSON());
        that.events = [];
        that.backend.send(events)
                    .then(result => { console.log(result); })
                    .catch(error => { console.error(error); });
    };
    private dispatching?: number;

    public get environment(): Environment {
        return Analytics.ENVIRONMENT;
    }

    public set environement(value: Environment) {
        Analytics.ENVIRONMENT = { ...Analytics.ENVIRONMENT, ...value };
    }

    constructor(backend: Backend, environment: Environment) {
        this.backend = backend;
        Analytics.ENVIRONMENT = { ...environment };
    }

    public startDispatching(interval: number): void {
        if (this.dispatching) { return; }
        this.dispatching = window.setInterval(this.dispatcher, interval, this);
    }

    public stopDispatching(): void {
        if (!this.dispatching) { return; }
        window.clearInterval(this.dispatching);
        this.dispatching = undefined;
    }

    public captureStart(key?: string): TimeMeasurement {
        const measurement = new TimeMeasurement().start();
        this.peformance[key ? key : measurement.id] = measurement;
        return measurement;
    }

    public captureEnd(key: string, details?: {}): void;
    public captureEnd(key: string, startOrDetails?: TimeMeasurement, details?: object): void {
        let value: number;
        let start: TimeMeasurement;
        let options: object;
        if (startOrDetails instanceof TimeMeasurement) {
            start = startOrDetails;
            options = details || {};
            value = start.end().value;
        } else {
            if (!this.peformance[key]) { return; }
            value = this.peformance[key].end().value;
            delete this.peformance[key];
            options = startOrDetails || {};
        }

       // Create event for measurement and capture it 
        const event = new AnalyticsEvent(AnalyticsEventCategory.PERF,
                                         key,
                                         value,
                                         {...Analytics.ENVIRONMENT, ...options});
        this.captureEvent(event);
    }

    public captureEvent(event: AnalyticsEvent): void;
    public captureEvent(key: string, value: AnalyticsEventValue, details: {}): void;
    public captureEvent(eventOrKey: AnalyticsEvent|string, value?: AnalyticsEventValue, details: {} = {}): void {
        if (eventOrKey instanceof AnalyticsEvent) {
            this.events.push(eventOrKey);
            return;
        }

        const event = new AnalyticsEvent(AnalyticsEventCategory.STAT,
                                         eventOrKey,
                                         value || '',
                                         {...Analytics.ENVIRONMENT, ...details});
        this.events.push(event);
    }
}