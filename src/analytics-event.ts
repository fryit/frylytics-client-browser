import * as Utils from './utils'
import { JSONObject } from './json';

export type AnalyticsEventValue = string|number;

export enum AnalyticsEventCategory {
    STAT = 'statistics',
    PERF = 'performance'
}

export class AnalyticsEvent {
    public static readonly VERSION = 1;

    public readonly capturedAt: number;                 // Time when event has been captured
    public readonly category: AnalyticsEventCategory;   // Category of the event, to filter easiely
    public readonly client: string;                     // User-Agent, client identifiecation eg. python-http, etc...
    public readonly id: string;                         // Unique ID of the event
    public readonly key: string;                        // Event name like 'logged-in', 'item-form-render'
    public readonly organisation: string;               // Organisation event belongs to
    public readonly project: string;                    // EB, Kaizen, Practique...
    public readonly release: string;                    // Source release identifier
    public readonly source: string;                     // Application, eg. 'eb-frontend', 'fry-oidc'...
    public readonly tags: {[K: string]: string};        // Arbitrary structured data
    public readonly user: {[K: string]: string};        // User information
    public readonly value: AnalyticsEventValue;         // Value of the metric eg. time etc..
    public readonly version: number;                    // Version of the Event object

    constructor(category: AnalyticsEventCategory,
                key: string,
                value: AnalyticsEventValue,
                details: any) {
        this.capturedAt = Date.now();
        this.category = category;
        this.client = details.client || '';
        this.id = Utils.ID();
        this.key = key;
        this.organisation = details.organisation || '';
        this.project = details.project || '';
        this.release = details.release || '';
        this.source =  details.source || '';
        this.tags = details.tags || {};
        this.user = details.user || {};
        this.value = value;
        this.version = AnalyticsEvent.VERSION;
    }

    public toJSON(): JSONObject {
        return {
            'capturedAt': this.capturedAt,
            'category': this.category,
            'client': this.client,
            'id': this.id,
            'key': this.key,
            'organisation': this.organisation,
            'project': this.project,
            'release': this.release,
            'source':  this.source,
            'tags': this.tags,
            'user': this.user,
            'value': this.value,
            'version': this.version
        }
    }
}