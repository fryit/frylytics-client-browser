export function ID(length: number = 32): string {
    return [...Array(length)].map(i=>(~~(Math.random()*36)).toString(36)).join('')
}