(function(f){if(typeof exports==="object"&&typeof module!=="undefined"){module.exports=f()}else if(typeof define==="function"&&define.amd){define([],f)}else{var g;if(typeof window!=="undefined"){g=window}else if(typeof global!=="undefined"){g=global}else if(typeof self!=="undefined"){g=self}else{g=this}g.Frylytics = f()}})(function(){var define,module,exports;return (function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var Utils = __importStar(require("./utils"));
var AnalyticsEventCategory;
(function (AnalyticsEventCategory) {
    AnalyticsEventCategory["STAT"] = "statistics";
    AnalyticsEventCategory["PERF"] = "performance";
})(AnalyticsEventCategory = exports.AnalyticsEventCategory || (exports.AnalyticsEventCategory = {}));
var AnalyticsEvent = (function () {
    function AnalyticsEvent(category, key, value, details) {
        this.capturedAt = Date.now();
        this.category = category;
        this.client = details.client || '';
        this.id = Utils.ID();
        this.key = key;
        this.organisation = details.organisation || '';
        this.project = details.project || '';
        this.release = details.release || '';
        this.source = details.source || '';
        this.tags = details.tags || {};
        this.user = details.user || {};
        this.value = value;
        this.version = AnalyticsEvent.VERSION;
    }
    AnalyticsEvent.prototype.toJSON = function () {
        return {
            'capturedAt': this.capturedAt,
            'category': this.category,
            'client': this.client,
            'id': this.id,
            'key': this.key,
            'organisation': this.organisation,
            'project': this.project,
            'release': this.release,
            'source': this.source,
            'tags': this.tags,
            'user': this.user,
            'value': this.value,
            'version': this.version
        };
    };
    AnalyticsEvent.VERSION = 1;
    return AnalyticsEvent;
}());
exports.AnalyticsEvent = AnalyticsEvent;

},{"./utils":6}],2:[function(require,module,exports){
"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var Backend = (function () {
    function Backend(options) {
        this.options = options;
    }
    Backend.prototype.endpointURL = function (endpoint, baseURL, stage) {
        var base = baseURL || this.options.baseURL;
        var env = stage || this.options.environment;
        return "" + base + env + "/" + endpoint;
    };
    Backend.prototype.send = function (event) {
        return __awaiter(this, void 0, void 0, function () { return __generator(this, function (_a) {
            return [2];
        }); });
    };
    return Backend;
}());
exports.Backend = Backend;
var AWSBackend = (function (_super) {
    __extends(AWSBackend, _super);
    function AWSBackend() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    AWSBackend.prototype.send = function (event) {
        return __awaiter(this, void 0, void 0, function () {
            var parameters, response, data, message;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        parameters = {
                            method: 'POST',
                            mode: 'cors',
                            cache: 'no-cache',
                            headers: { 'Content-Type': 'application/json' },
                            body: JSON.stringify(typeof event.length === 'number' ? event : [event])
                        };
                        return [4, fetch(this.endpointURL('events/'), parameters)];
                    case 1:
                        response = _a.sent();
                        return [4, response.json()];
                    case 2:
                        data = _a.sent();
                        if (!response.ok) {
                            message = response.statusText || data.error.message;
                            throw new Error(message);
                        }
                        return [2, data];
                }
            });
        });
    };
    return AWSBackend;
}(Backend));
exports.AWSBackend = AWSBackend;

},{}],3:[function(require,module,exports){
"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var analytics_event_1 = require("./analytics-event");
var time_measurement_1 = require("./time-measurement");
var Analytics = (function () {
    function Analytics(backend, environment) {
        this.events = [];
        this.peformance = {};
        this.dispatcher = function (that) {
            if (that.events.length === 0) {
                return;
            }
            var events = that.events.map(function (e) { return e.toJSON(); });
            that.events = [];
            that.backend.send(events)
                .then(function (result) { console.log(result); })
                .catch(function (error) { console.error(error); });
        };
        this.backend = backend;
        Analytics.ENVIRONMENT = __assign({}, environment);
    }
    Object.defineProperty(Analytics.prototype, "environment", {
        get: function () {
            return Analytics.ENVIRONMENT;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Analytics.prototype, "environement", {
        set: function (value) {
            Analytics.ENVIRONMENT = __assign(__assign({}, Analytics.ENVIRONMENT), value);
        },
        enumerable: true,
        configurable: true
    });
    Analytics.prototype.startDispatching = function (interval) {
        if (this.dispatching) {
            return;
        }
        this.dispatching = window.setInterval(this.dispatcher, interval, this);
    };
    Analytics.prototype.stopDispatching = function () {
        if (!this.dispatching) {
            return;
        }
        window.clearInterval(this.dispatching);
        this.dispatching = undefined;
    };
    Analytics.prototype.captureStart = function (key) {
        var measurement = new time_measurement_1.TimeMeasurement().start();
        this.peformance[key ? key : measurement.id] = measurement;
        return measurement;
    };
    Analytics.prototype.captureEnd = function (key, startOrDetails, details) {
        var value;
        var start;
        var options;
        if (startOrDetails instanceof time_measurement_1.TimeMeasurement) {
            start = startOrDetails;
            options = details || {};
            value = start.end().value;
        }
        else {
            if (!this.peformance[key]) {
                return;
            }
            value = this.peformance[key].end().value;
            delete this.peformance[key];
            options = startOrDetails || {};
        }
        var event = new analytics_event_1.AnalyticsEvent(analytics_event_1.AnalyticsEventCategory.PERF, key, value, __assign(__assign({}, Analytics.ENVIRONMENT), options));
        this.captureEvent(event);
    };
    Analytics.prototype.captureEvent = function (eventOrKey, value, details) {
        if (details === void 0) { details = {}; }
        if (eventOrKey instanceof analytics_event_1.AnalyticsEvent) {
            this.events.push(eventOrKey);
            return;
        }
        var event = new analytics_event_1.AnalyticsEvent(analytics_event_1.AnalyticsEventCategory.STAT, eventOrKey, value || '', __assign(__assign({}, Analytics.ENVIRONMENT), details));
        this.events.push(event);
    };
    Analytics.ENVIRONMENT = {
        client: window.navigator.userAgent,
        organisation: undefined,
        project: undefined,
        release: undefined,
        source: undefined,
        tags: {},
        user: {}
    };
    return Analytics;
}());
exports.Analytics = Analytics;

},{"./analytics-event":1,"./time-measurement":5}],4:[function(require,module,exports){
"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(require("./analytics-event"));
__export(require("./backend"));
__export(require("./client"));
__export(require("./time-measurement"));
__export(require("./utils"));

},{"./analytics-event":1,"./backend":2,"./client":3,"./time-measurement":5,"./utils":6}],5:[function(require,module,exports){
"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var Utils = __importStar(require("./utils"));
var TimeMeasurement = (function () {
    function TimeMeasurement(start, end) {
        if (start === void 0) { start = NaN; }
        if (end === void 0) { end = NaN; }
        this.id = Utils.ID(8);
        this._start = typeof start !== 'number' ? NaN : start;
        this._end = typeof end !== 'number' ? NaN : end;
    }
    TimeMeasurement.prototype.getStart = function () { return this._start; };
    TimeMeasurement.prototype.getEnd = function () { return this._end; };
    Object.defineProperty(TimeMeasurement.prototype, "value", {
        get: function () {
            return this._end - this._start;
        },
        enumerable: true,
        configurable: true
    });
    TimeMeasurement.prototype.start = function () {
        this._start = performance.now();
        return this;
    };
    TimeMeasurement.prototype.end = function () {
        this._end = performance.now();
        return this;
    };
    return TimeMeasurement;
}());
exports.TimeMeasurement = TimeMeasurement;

},{"./utils":6}],6:[function(require,module,exports){
"use strict";
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
Object.defineProperty(exports, "__esModule", { value: true });
function ID(length) {
    if (length === void 0) { length = 32; }
    return __spreadArrays(Array(length)).map(function (i) { return (~~(Math.random() * 36)).toString(36); }).join('');
}
exports.ID = ID;

},{}]},{},[4])(4)
});
