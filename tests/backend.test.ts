import { Environment } from '../src/client';
import { BackendOptions, AWSBackend } from '../src/backend';
import { AnalyticsEvent, AnalyticsEventCategory } from '../src/analytics-event';

describe('Backend', () => {
    const _options: BackendOptions = {
        baseURL: 'https://k1h3p9lorj.execute-api.eu-west-1.amazonaws.com/',
        environment: 'dev'
    }

    describe('AWSBackend', () => {
        let _backend: AWSBackend;
        beforeEach(() => {
            _backend = new AWSBackend(_options);
        });

        describe('send event', () => {
            it('has correct endpoint', () => {
                const url = _backend.endpointURL('events/');
                expect(url).toBe('https://k1h3p9lorj.execute-api.eu-west-1.amazonaws.com/dev/events/');
            });
            it('can send event successfuly', () => {
                const environment: Environment = {
                    client: 'jasmine-test',
                    project: 'eas',
                    release: '1.0.0',
                    source: 'eas-analytics',
                    organisation: 'fry',
                    user: {},
                    tags: {}
                };
                const event =
                    new AnalyticsEvent(AnalyticsEventCategory.STAT, 'some-key', 1, environment)

                return _backend.send(event.toJSON()).then(
                    (result) => {
                        expect(result.id.length).toBe(32);
                    }
                );
            });
        });
    });
});