import { TimeMeasurement } from '../src/time-measurement';

describe('TimeMeasurement', () => {
  let _measurement: TimeMeasurement;

  beforeEach(() => {
    _measurement = new TimeMeasurement();
  });

  it('is created providing the key which is prefix for ID', () => {
    expect(_measurement.id).toMatch(/^.{8}$/);
    expect(_measurement.getStart()).toBeNaN();
    expect(_measurement.getEnd()).toBeNaN();
  });

  it('is created with provided parmeters', () => {
    _measurement = new TimeMeasurement(1, 2);
    expect(_measurement.getStart()).toBe(1);
    expect(_measurement.getEnd()).toBe(2);
  });

  it('is created with defaults when invalid input is provided', () => {
    _measurement = new TimeMeasurement(null, undefined);
    expect(_measurement.getStart()).toBeNaN();
    expect(_measurement.getEnd()).toBeNaN();
  });

  it('start() captures start time', () => {
    expect(_measurement.start().getStart()).toBeGreaterThan(0);
  });

  it('end() captures end time', () => {
    expect(_measurement.end().getEnd()).toBeGreaterThan(0);
  });

  it('value provides time elapsed when start/end', () => {
    _measurement.start();
    _measurement.end();
    const expected = _measurement.getEnd() - _measurement.getStart();
    expect(_measurement.value).toEqual(expected);
  });

  it('value provides NaN when no start', () => {
    expect(_measurement.value).toBeNaN();
  });

  it('value provides NaN when start but no end', () => {
    _measurement.start();
    expect(_measurement.value).toBeNaN();
  });

  it('value provides NaN when no start but end', () => {
    _measurement.end();
    expect(_measurement.value).toEqual(NaN);
  });
});