import { AnalyticsEvent, AnalyticsEventCategory } from '../src/analytics-event';

describe('AnalyticsEvent', () => {
    describe('creation', () => {
        it('can be created with minimal data', () => {
            const event =
                new AnalyticsEvent(AnalyticsEventCategory.STAT, 'some-key', 1, {})
            expect(event.capturedAt).toBeDefined();
            expect(event.category).toBe(AnalyticsEventCategory.STAT);
            expect(event.client).toBe('');
            expect(event.id.length).toBe(32);
            expect(event.key).toBe('some-key');
            expect(event.project).toBe('');
            expect(event.release).toBe('');
            expect(event.source).toBe('');
            expect(event.tags).toEqual({});
            expect(event.user).toEqual({});
            expect(event.value).toBe(1);
            expect(event.version).toBe(AnalyticsEvent.VERSION);
        });
    });
});